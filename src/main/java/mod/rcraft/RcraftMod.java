package mod.rcraft;

import net.minecraftforge.fml.relauncher.Side;
import net.minecraftforge.fml.common.registry.GameRegistry;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.IGuiHandler;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.SidedProxy;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.Instance;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.IWorldGenerator;
import net.minecraftforge.fml.common.IFuelHandler;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.client.model.obj.OBJLoader;

import net.minecraft.world.chunk.IChunkProvider;
import net.minecraft.world.chunk.IChunkGenerator;
import net.minecraft.world.World;
import net.minecraft.item.ItemStack;
import net.minecraft.entity.player.EntityPlayer;

import java.util.Random;

@Mod(modid = RcraftMod.MODID, version = RcraftMod.VERSION)
public class RcraftMod implements IFuelHandler, IWorldGenerator {

	public static final String MODID = "rcraft";
	public static final String VERSION = "0.1";

	@SidedProxy(clientSide = "mod.rcraft.ClientProxyRcraft", serverSide = "mod.rcraft.CommonProxyRcraft")
	public static CommonProxyRcraft proxy;

	@Instance(MODID)
	public static RcraftMod instance;

	Rcraft_rcraft rcraft_0 = new Rcraft_rcraft();
	Rcraft_boat rcraft_1 = new Rcraft_boat();
	Rcraft_gater2 rcraft_2 = new Rcraft_gater2();
	Rcraft_gater1 rcraft_3 = new Rcraft_gater1();
	Rcraft_lios rcraft_4 = new Rcraft_lios();
	Rcraft_boat2 rcraft_5 = new Rcraft_boat2();
	Rcraft_boat3 rcraft_6 = new Rcraft_boat3();
	Rcraft_decoflag1 rcraft_7 = new Rcraft_decoflag1();

	@Override
	public int getBurnTime(ItemStack fuel) {
		if (rcraft_0.addFuel(fuel) != 0)
			return rcraft_0.addFuel(fuel);
		if (rcraft_1.addFuel(fuel) != 0)
			return rcraft_1.addFuel(fuel);
		if (rcraft_2.addFuel(fuel) != 0)
			return rcraft_2.addFuel(fuel);
		if (rcraft_3.addFuel(fuel) != 0)
			return rcraft_3.addFuel(fuel);
		if (rcraft_4.addFuel(fuel) != 0)
			return rcraft_4.addFuel(fuel);
		if (rcraft_5.addFuel(fuel) != 0)
			return rcraft_5.addFuel(fuel);
		if (rcraft_6.addFuel(fuel) != 0)
			return rcraft_6.addFuel(fuel);
		if (rcraft_7.addFuel(fuel) != 0)
			return rcraft_7.addFuel(fuel);
		return 0;
	}

	@Override
	public void generate(Random random, int chunkX, int chunkZ, World world, IChunkGenerator chunkGenerator, IChunkProvider chunkProvider) {

		chunkX = chunkX * 16;
		chunkZ = chunkZ * 16;
		if (world.provider.getDimension() == -1)
			rcraft_0.generateNether(world, random, chunkX, chunkZ);
		if (world.provider.getDimension() == 0)
			rcraft_0.generateSurface(world, random, chunkX, chunkZ);
		if (world.provider.getDimension() == -1)
			rcraft_1.generateNether(world, random, chunkX, chunkZ);
		if (world.provider.getDimension() == 0)
			rcraft_1.generateSurface(world, random, chunkX, chunkZ);
		if (world.provider.getDimension() == -1)
			rcraft_2.generateNether(world, random, chunkX, chunkZ);
		if (world.provider.getDimension() == 0)
			rcraft_2.generateSurface(world, random, chunkX, chunkZ);
		if (world.provider.getDimension() == -1)
			rcraft_3.generateNether(world, random, chunkX, chunkZ);
		if (world.provider.getDimension() == 0)
			rcraft_3.generateSurface(world, random, chunkX, chunkZ);
		if (world.provider.getDimension() == -1)
			rcraft_4.generateNether(world, random, chunkX, chunkZ);
		if (world.provider.getDimension() == 0)
			rcraft_4.generateSurface(world, random, chunkX, chunkZ);
		if (world.provider.getDimension() == -1)
			rcraft_5.generateNether(world, random, chunkX, chunkZ);
		if (world.provider.getDimension() == 0)
			rcraft_5.generateSurface(world, random, chunkX, chunkZ);
		if (world.provider.getDimension() == -1)
			rcraft_6.generateNether(world, random, chunkX, chunkZ);
		if (world.provider.getDimension() == 0)
			rcraft_6.generateSurface(world, random, chunkX, chunkZ);
		if (world.provider.getDimension() == -1)
			rcraft_7.generateNether(world, random, chunkX, chunkZ);
		if (world.provider.getDimension() == 0)
			rcraft_7.generateSurface(world, random, chunkX, chunkZ);

	}

	@EventHandler
	public void load(FMLInitializationEvent event) {

		GameRegistry.registerFuelHandler(this);
		GameRegistry.registerWorldGenerator(this, 1);
		if (event.getSide() == Side.CLIENT) {
			OBJLoader.INSTANCE.addDomain("rcraft");
		}
		NetworkRegistry.INSTANCE.registerGuiHandler(this, new GuiHandler());
		rcraft_0.load(event);
		rcraft_1.load(event);
		rcraft_2.load(event);
		rcraft_3.load(event);
		rcraft_4.load(event);
		rcraft_5.load(event);
		rcraft_6.load(event);
		rcraft_7.load(event);
		proxy.registerRenderers(this);

	}

	@EventHandler
	public void serverLoad(FMLServerStartingEvent event) {
		rcraft_0.serverLoad(event);
		rcraft_1.serverLoad(event);
		rcraft_2.serverLoad(event);
		rcraft_3.serverLoad(event);
		rcraft_4.serverLoad(event);
		rcraft_5.serverLoad(event);
		rcraft_6.serverLoad(event);
		rcraft_7.serverLoad(event);
	}

	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		rcraft_0.instance = this.instance;
		rcraft_1.instance = this.instance;
		rcraft_2.instance = this.instance;
		rcraft_3.instance = this.instance;
		rcraft_4.instance = this.instance;
		rcraft_5.instance = this.instance;
		rcraft_6.instance = this.instance;
		rcraft_7.instance = this.instance;
		rcraft_0.preInit(event);
		rcraft_1.preInit(event);
		rcraft_2.preInit(event);
		rcraft_3.preInit(event);
		rcraft_4.preInit(event);
		rcraft_5.preInit(event);
		rcraft_6.preInit(event);
		rcraft_7.preInit(event);

	}

	public static class GuiHandler implements IGuiHandler {
		@Override
		public Object getServerGuiElement(int id, EntityPlayer player, World world, int x, int y, int z) {
			return null;
		}

		@Override
		public Object getClientGuiElement(int id, EntityPlayer player, World world, int x, int y, int z) {
			return null;
		}
	}

}