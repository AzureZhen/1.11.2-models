package mod.rcraft;

public class ClientProxyRcraft extends CommonProxyRcraft {

	@Override
	public void registerRenderers(RcraftMod ins) {
		ins.rcraft_0.registerRenderers();
		ins.rcraft_1.registerRenderers();
		ins.rcraft_2.registerRenderers();
		ins.rcraft_3.registerRenderers();
		ins.rcraft_4.registerRenderers();
		ins.rcraft_5.registerRenderers();
		ins.rcraft_6.registerRenderers();
		ins.rcraft_7.registerRenderers();
	}
}